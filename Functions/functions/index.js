const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();
// The Firebase Admin SDK to access Firestore.
var db = admin.firestore();

const {
  newVacancyLogController,
} = require('./controllers/newVacancyLogController.js');

const {
  newCommentLogController,
} = require('./controllers/newCommentLogController.js');

const {
  favoritesDeleteController,
} = require('./controllers/favoritesDeleteController.js');

if (process.env.local) {
  const { emulateInserts } = require('./controllers/emulateInserts.js');
  exports.testFunctions = functions.https.onRequest(emulateInserts);
}

// Log when user create new vacancy
exports.newVacancyLog = functions.firestore
  .document('/vacancies/{documentId}')
  .onCreate(newVacancyLogController);

// Log when user update any comments
exports.newCommentLog = functions.firestore
  .document('/vacancies/{documentId}')
  .onUpdate(newCommentLogController);

// Log when user update any comments
exports.favoritesDeleteController = functions.firestore
  .document('/favorites/{documentId}')
  .onDelete(favoritesDeleteController);
