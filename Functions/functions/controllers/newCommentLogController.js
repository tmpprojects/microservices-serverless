const functions = require('firebase-functions');
var db = require('firebase-admin').firestore();
const {logsCode} = require('./logsCode');


exports.newCommentLogController = (change, context) => {
  // console.log(context);
  // Grab the current value of what was written to Firestore.
  try {
    const previousValue = change.before.data();
    const newValue = change.after.data();

    // functions.logger.info('newValue----------------------------------------------------', newValue);
    if (newValue.comments.length < previousValue.comments.length) {
      //functions.logger.log("-------->", newValue);
      // functions.logger.info('-------->SNAP:', snap);
      functions.logger.info('deleteComment:', context);
      //functions.logger.error("-------->", newValue);
      return db.collection('logs').add({
        owner: {
          idUser: newValue.owner.idUser,
          name: newValue.owner.name,
          email: newValue.owner.email,
        },
        id: context.params.documentId,
        parentId: context.params.documentId,
        title: newValue.title,
        tags: newValue.tags,
        collection: 'vacancies.comments',
        action: logsCode.deleteComment.name,
        actionCode: logsCode.deleteComment.code,
        createdAt: context.timestamp,
        dangerous: 'none',
      });
    } else if (newValue.comments.length !== previousValue.comments.length) {
      //functions.logger.log("-------->", newValue);
      // functions.logger.info('-------->SNAP:', snap);
      functions.logger.info('newCommentLog:', context);
      //functions.logger.error("-------->", newValue);
      return db.collection('logs').add({
        owner: {
          idUser: newValue.owner.idUser,
          name: newValue.owner.name,
          email: newValue.owner.email,
        },
        id: context.params.documentId,
        parentId: context.params.documentId,
        title: newValue.title,
        tags: newValue.tags,
        collection: 'vacancies.comments',
        action: logsCode.newComment.name,
        actionCode: logsCode.newComment.code,
        createdAt: context.timestamp,
        dangerous: 'none',
      });
    }
  } catch (e) {
    return functions.logger.error(e.message);
  }
  //console.log(newValue);
  // Access the parameter `{documentId}` with `context.params`
  //functions.logger.log('Uppercasing', context.params.documentId, newValue);
  //const uppercase = newValue.toUpperCase();
  //console.log(newValue);

  // You must return a Promise when performing asynchronous tasks inside a Functions such as
  // writing to Firestore.
  // Setting an 'uppercase' field in Firestore document returns a Promise.
  //return snap.ref.set({ uppercase }, { merge: true });
  // return true;
};
