exports.logsCode = {
  newVacancy: {
    name: 'Add new vacancy',
    code: '01',
  },
  newComment: {
    name: 'Add new comment',
    code: '02',
  },
  deleteComment: {
    name: 'Delete a comment',
    code: '03',
  },
};
