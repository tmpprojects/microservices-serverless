'use strict';

const functions = require('firebase-functions');
var db = require('firebase-admin').firestore();
const { logsCode } = require('./logsCode.js');

// const cors = require('cors');

const model = require('/Volumes/DESARROLLO/SERVERLESS/VacantesDigitales/Microservicios/Admin/Company/CreateVacancy/model.js');

//const express = require('express');
//const app = express();

// app.use(express.urlencoded({ extended: true, strict: false }));
// app.use(express.json());
// app.use(cors());

exports.emulateInserts = async (req, res) => {
  // const defaultLimit = 1;
  // const limit = req.query.limit ? req.query.limit : defaultLimit;
  // const created = [];
  // for (let r = 0; r < limit; r++){
  let write = model.createVacancyData.postFakeID;
  if (req.query.id) {
    write = req.query.id;
    model.createVacancyData.postFakeID = write;
  }
  db.collection('vacancies')
    .doc(write)
    .set(model.createVacancyData)
    .then(function (docRef) {
      //created.push({ id: docRef.id });
      // res.status(201).json('created '+model.createVacancyData.postFakeID);
    })
    .catch(function (error) {
      //res.status(201).json(error.message);
    });
  //};

  res.json({ 'insert:': model.createVacancyData.postFakeID });
};
