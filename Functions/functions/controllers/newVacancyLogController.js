// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.

const functions = require('firebase-functions');
const db = require('firebase-admin').firestore();
const slug = require('slug');
const { FirebaseDynamicLinks } = require('firebase-dynamic-links');
const firebaseDynamicLinks = new FirebaseDynamicLinks(
  'AIzaSyDNgLPlF4aFEjSJWEKFn75vSTfbnIC1ddc',
);

const { logsCode } = require('./logsCode');

exports.newVacancyLogController = async (snap, context) => {
  // console.log(context);
  // Grab the current value of what was written to Firestore.
  try {
    const original = snap.data();

    //functions.logger.log("-------->", original);
    // functions.logger.info('-------->SNAP:', snap);
    functions.logger.info(original);
    //functions.logger.error("-------->", original);
    db.collection('logs').add({
      owner: {
        idUser: original.owner.idUser,
        name: original.owner.name,
        email: original.owner.email,
      },
      id: context.params.documentId,
      title: original.title,
      collection: 'vacancies',
      tags: original.tags,
      action: logsCode.newVacancy.name,
      actionCode: logsCode.newVacancy.code,
      createdAt: context.timestamp,
      dangerous: 'none',
    });
    ////////////////////////////
    //Create shortLink:
    try {
      if (Boolean(original.company.website)) {
        const { shortLink, previewLink } = await firebaseDynamicLinks.createLink({
          longDynamicLink: `https://vacantesdigitales.page.link/?link=${original.company.website}`,
          suffix: {
            option: 'UNGUESSABLE',
          },
        });

        await db
          .collection('vacancies')
          .doc(context.params.documentId)
          .set(
            {
              company: {
                websiteLink: shortLink,
              },
            },
            { merge: true },
          );
      }
      ////////////Create slug & link slug

      const setSlug = `${slug(original.title)}-${context.params.documentId}`;
      const { shortLink, previewLink } = await firebaseDynamicLinks.createLink({
        longDynamicLink: `https://vacantesdigitales.page.link/?link=http://vacantesdigitales.com/vacante/${setSlug}`,
        suffix: {
          option: 'UNGUESSABLE',
        },
      });

      await db
        .collection('vacancies')
        .doc(context.params.documentId)
        .set(
          {
            linkShare: shortLink,
            slug: setSlug,
          },
          { merge: true },
        );
    } catch (e) {
      console.log(e.message);
    }

    return true;
  } catch (e) {
    return functions.logger.error(e.message);
  }
  //console.log(original);
  // Access the parameter `{documentId}` with `context.params`
  //functions.logger.log('Uppercasing', context.params.documentId, original);
  //const uppercase = original.toUpperCase();
  //console.log(original);

  // You must return a Promise when performing asynchronous tasks inside a Functions such as
  // writing to Firestore.
  // Setting an 'uppercase' field in Firestore document returns a Promise.
  //return snap.ref.set({ uppercase }, { merge: true });
  // return true;
};
