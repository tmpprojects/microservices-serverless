exports.dangerCode = {
  level0: {
    name: 'None',
    code: 'Is not dangerous',
  },
  level1: {
    name: 'Small',
    code: 'Maybe is dangerous',
  },
  level2: {
    name: 'Medium',
    code: 'Check if is dangerous',
  },
  level3: {
    name: 'hight',
    code: 'Is very dangerous',
  },
};
