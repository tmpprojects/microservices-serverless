'use strict';

const cors = require('cors');

const querystring = require('querystring');
const serverless = require('serverless-http');
const express = require('express');
const app = express();
const AWS = require('aws-sdk');
const bodyParser = require('body-parser');
const moment = require('moment'); // require
require("moment-timezone"); // require
moment.locale('es-MX');
moment.tz.setDefault('America/Mexico_City');

const fireLib = require(process.env.IS_OFFLINE
  ? process.env.FIREBASE_MODULE
  : "firebaseServices");
app.use(express.urlencoded({ extended: true, strict: false }));
app.use(express.json());
app.use(cors());

moment.locale('es-MX');
const OFFLINE = process.env.IS_OFFLINE;
let priorityMissing;

function setParam(param = false, type = 'none', priority = false, field = '') {
  const ifexist = param ? true : false;
  if (!ifexist && priority == true) {
    priorityMissing.push(field);
  }
  switch (type) {
    case 'none':
      if (ifexist) {
        return param;
      }
      break;
    case 'Int':
      if (ifexist) {
        return parseInt(param);
      }
      break;
    case 'Obj':
      if (ifexist) {
        return JSON.parse(param);
      }
      break;
    default:
  }
  return null;
}

app.get('/', async (req, res) => {
  res.json({ message: 'There is no such method.' });
});
app.post('/', async (req, res) => {
  try {
    priorityMissing = [];
    const {
      title,
      location,
      modality,
      description,
      salary,
      company,
      tags,
      categories,
      image,
      customImage,
      userId
    } = req.body;

    /*
    Priority fields
    title, location, description, company, categories
    */

    const dataVacancy = {
      public: true,
      title: setParam(title, 'none', true, 'title'),
      location: setParam(location, 'Obj', true, 'location'),
      modality: setParam(modality, 'Obj', false, 'modality'),
      description: setParam(description, 'none', true, 'description'),
      salary: setParam(salary, 'Int', false),
      ranking: 0,
      averageRanking: 0,
      maxRanking: 100,
      unixdate: moment().format('X'),
      date: moment().format();,
      humanDate: moment().format('DD/MM/YY HH:mm:ss'),
      company: setParam(company, 'Obj', true, 'company'),
      tags: setParam(tags, 'Obj', false, 'tags'),
      categories: setParam(categories, 'Obj', true, 'categories'),
      subscription: null,
      lastModified:null,
      /*
      {
        "premium": false,
        "premiumpack": 0,
        "premiumdescription": "Ningúna suscripción."
        }
      */
      comments: null,
      likes: null,
      image: setParam(image, 'none', false, 'image'),
      customImage: setParam(customImage, 'none', false, 'customImage'),
      share: null,
      userId: setParam(userId, 'none', true, 'userId'),
    };

    if (priorityMissing.length > 0) {
      res.status(400).json({
        error: 'One or more priority fields is needed.',
        fields: priorityMissing,
      });
    } else {
      const resData = await fireLib.db.collection('vacancies').add(dataVacancy);
      res.status(201).json({ id: resData.id });
    }
  } catch (e) {
    console.log(e);
    res.status(400).json({ error: e.message });
  }
});

module.exports.setNewVacancy = serverless(app);
