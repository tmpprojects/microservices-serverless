'use strict';

const cors = require('cors');
const fireLib =  require(process.env.IS_OFFLINE
  ? process.env.FIREBASE_MODULE
  : "firebaseServices");
const querystring = require('querystring');
const serverless = require('serverless-http');
const express = require('express');
const app = express();
const AWS = require('aws-sdk');
const bodyParser = require('body-parser');

const moment = require('moment'); // require
require("moment-timezone"); // require
moment.locale('es-MX');
moment.tz.setDefault('America/Mexico_City');

app.use(express.urlencoded({ extended: true, strict: false }));
app.use(express.json());
app.use(cors());


const OFFLINE = process.env.IS_OFFLINE;
let priorityMissing;

app.get('/', async (req, res) => {
  res.json({ message: 'There is no such method.' });
});

app.patch('/', async (req, res) => {
  try {
    const id = req.body.id;
    const userId = req.body.userId;

    const bodyObj = req.body;

    delete bodyObj.userId;
    delete bodyObj.id;

    bodyObj.ranking  = bodyObj.ranking ? parseInt(bodyObj.ranking) : null;
    bodyObj.averageRanking = bodyObj.averageRanking ? parseInt(bodyObj.averageRanking) : null;
    bodyObj.maxRanking = bodyObj.maxRanking ? parseInt(bodyObj.maxRanking) : null;
    bodyObj.salary = bodyObj.salary ? parseInt(bodyObj.salary)  : null ;
    bodyObj.maxRanking = bodyObj.maxRanking ? parseInt(bodyObj.maxRanking)  : null;
    bodyObj.likes = bodyObj.likes  ? parseInt(bodyObj.likes) : null;

    bodyObj.lastModified = moment().format();

    console.log("Save --- > ", bodyObj)
    // res.status(201).json(id);

    let exist = false;
    let newDocument;
    if (id) {
      let vacancyRef = await fireLib.db.collection('vacancies').doc(id);
      await vacancyRef.get().then((doc) => {
        if (doc.exists) {
          if (doc.data().userId == userId) {
            exist = true;
          } else {
            throw new Error('Unauthorized user.');
          }
        }
      });

      if (exist) {
        await fireLib.db
          .collection('vacancies')
          .doc(id)
          .set(bodyObj, { merge: true });

        await vacancyRef.get().then((doc) => {
          if (doc.exists) {
            if (doc.data().userId == userId) {
              newDocument = doc.data();
            } else {
              throw new Error('Unauthorized user.');
            }
          }
        });

        res.status(200).json(newDocument);
      } else {
        throw new Error("There's no vacancy.");
      }
    }
  } catch (e) {
    console.log(e);
    res.status(400).json({ error: e.message });
  }
});

module.exports.modifyVacancy = serverless(app);
