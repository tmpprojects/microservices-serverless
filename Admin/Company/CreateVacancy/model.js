const faker = require('faker');
const postFakeID = () => {
  return faker.random.alphaNumeric(20);
};

faker.locale = 'es_MX';

const registerUser = {
  idUser: 'UaI47TbRkpEGu5Nos4fD',
  name: 'Christian Irack',
  title: 'Fullstack developer',
  description: 'Apasionado del lenguaje de programación',
  sectors: ['development', 'designer', 'technology'],
  userName: 'christianirack',
  imageProfile:
    'https://canastarosa.s3.amazonaws.com/media/__sized__/users/ddb6b8d552433c81d1eda5775ae72f81-crop-c0-5__0-5-480x480.png',
  type: 'candidate',
  email: 'christian@irack.mx',
  maskEmail: 'chris98@vdigitales.com',
  twitter: '@christianirack',
  instagram: '@christianirack',
  linkedin: 'https://linkedin.com/christianirack',
  website: 'https://irack.mx',
  mobile: 5551555884,
  creditCard: false,
  memberSince: faker.date.recent(),
  gender: 'Masculino',
  birth: faker.date.past(),
  location: 'México',
  lat: 19.0,
  lgt: 9.3393,
  suscription: false,
};

const groupCategory = {
  name: 'Marketing Digital',
  level: 0,
  id: 0,
};

const groupCategories = [
  {
    name: 'Marketing Digital',
    level: 1,
    id: 1,
  },
  {
    name: 'Marketing',
    level: 0,
    id: 0,
  },
];

const fakeComments = [
  {
    owner: {
      name: `${faker.name.firstName()} ${faker.name.lastName()}`,
      title: 'Programmer',
      imageProfile: registerUser.imageProfile,
      idUser: registerUser.idUser,
      email: registerUser.email,
    },
    comment: '** Este es un comentario de mi propio usuario',
    idComment: faker.random.uuid(),
    parentId: postFakeID(),
    createAt: faker.date.past(),
  },
  {
    owner: {
      name: `${faker.name.firstName()} ${faker.name.lastName()}`,
      title: 'Programmer',
      imageProfile: faker.image.cats(),
      idUser: faker.random.uuid(),
      email: faker.internet.email(),
    },

    comment: faker.lorem.words(faker.random.number(15)),
    idComment: faker.random.uuid(),
    parentId: postFakeID(),
    createAt: faker.date.past(),
  },
];

const fakeCompany = {
  description: faker.company.catchPhraseDescriptor(400),
  email: faker.internet.email(),
  emailMask: 'mask@vdigitales.com',
  name: faker.company.companyName(),
  sectors: ['Tecnología', 'Innovación', 'Consultoría'],
  category: groupCategory,
  website: faker.internet.url(),
  logo: faker.image.cats(),
  cover: faker.image.business(),
  anonymous: faker.random.boolean(),

  createAt: faker.date.recent(),
  updateAt: faker.date.recent(),
};

const fakeFavorites = [
  {
    owner: {
      name: registerUser.name,
      title: 'Designer',
      idUser: registerUser.idUser,
      imageProfile: registerUser.imageProfile,
      email: registerUser.email,
    },
    createAt: faker.date.past(),
    parentId: postFakeID(),
  },
  {
    owner: {
      name: `${faker.name.firstName()} ${faker.name.lastName()}`,
      title: 'Programmer',
      idUser: faker.random.uuid(),
      imageProfile: faker.image.cats(),
      email: faker.internet.email(),
    },
    createAt: faker.date.past(),
    parentId: postFakeID(),
  },
  {
    owner: {
      name: `${faker.name.firstName()} ${faker.name.lastName()}`,
      title: 'DevOps',
      idUser: faker.random.uuid(),
      imageProfile: faker.image.cats(),
      email: faker.internet.email(),
    },
    createAt: faker.date.past(),
    parentId: postFakeID(),
  },
];

const fakeLikes = [
  {
    owner: {
      name: registerUser.name,
      title: 'Designer',
      idUser: registerUser.idUser,
      imageProfile: registerUser.imageProfile,
      email: registerUser.email,
    },
    createAt: faker.date.past(),
    parentId: postFakeID(),
  },
  {
    owner: {
      name: `${faker.name.firstName()} ${faker.name.lastName()}`,
      title: 'Programmer',
      idUser: faker.random.uuid(),
      imageProfile: faker.image.cats(),
      email: faker.internet.email(),
    },
    createAt: faker.date.past(),
    parentId: postFakeID(),
  },
  {
    owner: {
      name: `${faker.name.firstName()} ${faker.name.lastName()}`,
      title: 'DevOps',
      idUser: faker.random.uuid(),
      imageProfile: faker.image.cats(),
      email: faker.internet.email(),
    },
    createAt: faker.date.past(),
    parentId: postFakeID(),
  },
];

const fakeLocation = {
  city: faker.address.city(),
  country: faker.address.county(),
  zone: faker.address.citySuffix(),
  cp: faker.address.zipCode(),
  zoneImage: faker.image.city(),
  idLocation: faker.random.uuid(),
};

const fakeSalary = {
  min: faker.random.number(14000),
  max: faker.random.number(30000),
  set: faker.random.number(70000),
};

const fakeTags = ['marketing digital', 'marketing', 'publicidad'];

const fakeApplyes = [
  {
    owner: {
      name: `${faker.name.firstName()} ${faker.name.lastName()}`,
      title: 'Programmer',
      imageProfile: registerUser.imageProfile,
      idUser: registerUser.idUser,
      email: registerUser.email,
    },
    createAt: faker.date.past(),
    parentId: postFakeID(),
  },
  {
    owner: {
      name: faker.internet.userName(),
      title: 'Programmer',
      idUser: faker.random.uuid(),
      imageProfile: faker.image.cats(),
      email: registerUser.email,
    },
    createAt: faker.date.recent(),
    parentId: postFakeID(),
  },
  {
    owner: {
      name: faker.internet.userName(),
      idUser: faker.random.uuid(),
      imageProfile: faker.image.cats(),
    },
    createAt: faker.date.recent(),
    parentId: postFakeID(),
  },
];

///////// Model
//const RandomTitle = faker.name.jobTitle();
const RandomTitle = 'éste es un ·$%&/(()/&/)(!ª)= mensaje típico con 😁 memes';
const createVacancyData = {
  //props
  polularVacant: 7, // 0 to 10.
  averageRanking: faker.random.number(99),
  visits: faker.random.number(200), //show more

  category: groupCategory, //principal category
  subCategories: groupCategories,

  //comments: fakeComments,
  company: fakeCompany,

  //image and custom image
  postImage: faker.image.image(),
  uploadImage: faker.image.people(),

  //description
  salary: fakeSalary,
  title: RandomTitle,
  subtitle: faker.random.number(9) + ' años como mínimo.',
  modality: 'remote', // 'remote', 'inplace'
  description: `<strong>es mi nombre en bold </strong>${faker.lorem.words(
    300,
  )} <b>finaliza con bold</b>`,

  //likes: fakeLikes,
  location: fakeLocation,
  tags: fakeTags,

  //candidatesApplied: fakeApply,

  enabledComments: true,
  enabledLikes: true,
  publish: true,
  premium: false,
  levelPremium: false,

  createAt: faker.date.recent(),
  updateAt: faker.date.recent(),
  postFakeID: postFakeID(),
  owner: {
    name: `${faker.name.firstName()} ${faker.name.lastName()}`,
    title: 'Programmer',
    imageProfile: registerUser.imageProfile,
    idUser: registerUser.idUser,
    email: 'christian@irack.mx',
  },
};

module.exports = {
  createVacancyData,
  fakeFavorites,
  fakeLikes,
  fakeComments,
  fakeApplyes
};
