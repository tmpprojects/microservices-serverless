'use strict';
const cors = require('cors');
const fireLib = require(process.env.IS_OFFLINE
  ? process.env.FIREBASE_MODULE
  : 'firebaseServices');
const model = require('./model.js');
const serverless = require('serverless-http');
const express = require('express');
const app = express();
const moment = require('moment'); // require
require('moment-timezone'); // require
moment.locale('es-MX');
moment.tz.setDefault('America/Mexico_City');

app.use(express.urlencoded({ extended: true, strict: false }));
app.use(express.json());
app.use(cors());

app.get('/', async (req, res) => {
  try {
    const defaultLimit = 1;
    const limit = req.query.limit ? req.query.limit : defaultLimit;
    const created = [];
    let dynamicID = model.createVacancyData.postFakeID;
    if (req.query.id) {
      dynamicID = req.query.id;
    }

    //for (let r = 0; r < limit; r++) {
    const refRootDocument = fireLib.db.collection('vacancies');
    await refRootDocument
      .add(model.createVacancyData)
      .then(async (docRef) => {
        //created.push({ id: docRef.id });

        model.fakeFavorites.forEach((item) => {
          refRootDocument
            .doc(docRef.id)
            .collection('favorites')
            .doc(item.owner.idUser)
            .set({...item, parentId: docRef.id });
        });

        model.fakeLikes.forEach((item) => {
          refRootDocument
            .doc(docRef.id)
            .collection('likes')
            .doc(item.owner.idUser)
            .set({...item, parentId: docRef.id });
        });

        model.fakeComments.forEach((item) => {
          refRootDocument
            .doc(docRef.id)
            .collection('comments')
            .doc(item.owner.idUser)
            .set({...item, parentId: docRef.id });
        });

       
        model.fakeApplyes.forEach((item) => {
          refRootDocument
            .doc(docRef.id)
            .collection('applyes')
            .doc(item.owner.idUser)
            .set({...item, parentId: docRef.id });
        });
        

        res.status(201).json('created ' + dynamicID);
      })
      .catch(function (error) {
        res.status(201).json(error.message);
      });
    // }
  } catch (e) {
    console.log(e);
    res.status(400).json({ error: e.message });
  }
});

module.exports.createVacancy = serverless(app);
