const { createCanvas, registerFont, loadImage } = require('canvas');
const fs = require('fs');
function randomIntFromInterval(min, max) {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}



const baseX = 1800;
const baseY = 1200;

function createVacancy(render) {
  try {
    let render = {
        title: 'Experto en marketing digital!',
        subtitle: 'Para importante empresa en CDMX',
        email: 'christian@cvdigital.app',
      };
    console.log('*************+ createVacancy');
    registerFont('./fonts/helvetica-bold.otf', { family: 'VacantesDigitales' });
    registerFont('./fonts/Arial.ttf', { family: 'VacantesDigitales2' });
    const canvas = createCanvas(baseX, baseY, 'png');
    const ctx = canvas.getContext('2d');

    loadImage(`./backgrounds/image${randomIntFromInterval(0, 10)}.png`).then(
      (image) => {
        ctx.drawImage(image, 0, 0, baseX, baseY);
        //console.log('<img src="' + canvas.toDataURL() + '" />')

        addText(ctx, render.title, 100, 0, 'VacantesDigitales');

        addText(ctx, render.subtitle, 60, 80, 'VacantesDigitales');

        const topEmail = 270;
        addText(ctx, 'Aplica en', 40, topEmail, 'VacantesDigitales');
        addText(ctx, render.email, 40, topEmail + 60, 'VacantesDigitales', 'white');
        addText(
          ctx,
          'Más información en vacantesdigitales.com',
          30,
          topEmail + 260,
          'VacantesDigitales2',
        );
        loadImage('./logos/logo2.png').then((image) => {
          ctx.drawImage(image, 1400, 60, 325, 115);
          fs.writeFileSync('./out/out.png', canvas.toBuffer());
        });
      },
    );

    function addText(
      ctx,
      text,
      size,
      addTop = 0,
      font = 'VacantesDigitales',
      color = 'black',
    ) {
      //const text = "especialista en goolge analytics";
      ctx.font = `${size}px ${font}`;
      //ctx.rotate(0.1)
      const { width: width, actualBoundingBoxAscent: height } = ctx.measureText(
        text,
      );
      const centerX = (baseX - width) / 2;
      const centerY = (baseY - height) / 2;

      if (color === 'black') {
        ctx.fillStyle = 'rgba(0,0,0,.8)';
      } else {
        ctx.fillStyle = 'rgba(255,255,255,.9)';
      }

      ctx.fillText(text, centerX, centerY + addTop);
      // Draw line under text
    }
  } catch (e) {
    console.log(e);
  }
}
module.exports = createVacancy;
