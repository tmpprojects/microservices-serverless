'use strict';
const render = require('./render');
//const serverless = require('serverless-http');
var http = require('http');
let execute = false;

http
    .createServer(function (req, res) {
      res.write('Hello World!'); //write a response to the client
      console.log('crear', execute);
      if (execute) {
        render({
          title: 'Experto en marketing digital!',
          subtitle: 'Para importante empresa en CDMX',
          email: 'christian@cvdigital.app',
        });
      }
      execute = true;

      res.end(); //end the response
    })
    .listen(8080); //the server object listens on port 8080