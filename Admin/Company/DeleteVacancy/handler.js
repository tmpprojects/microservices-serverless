'use strict';

const cors = require('cors');
const fireLib =  require(process.env.IS_OFFLINE
  ? process.env.FIREBASE_MODULE
  : "firebaseServices");
const querystring = require('querystring');
const serverless = require('serverless-http');
const express = require('express');
const app = express();
const AWS = require('aws-sdk');
const bodyParser = require('body-parser');
const moment = require('moment'); // require
require("moment-timezone"); // require
moment.locale('es-MX');
moment.tz.setDefault('America/Mexico_City');

app.use(express.urlencoded({ extended: true, strict: false }));
app.use(express.json());
app.use(cors());
const OFFLINE = process.env.IS_OFFLINE;
let priorityMissing;

app.get('/', async (req, res) => {
  res.json({ message: 'There is no such method.' });
});
app.post('/', async (req, res) => {
  res.json({ message: 'There is no such method.' });
});

app.delete('/', async (req, res) => {
  try {
    const id = req.body.id;
    const userId = req.body.userId;
    let exist = false;
    if (id) {
      let vacancyRef = await fireLib.db.collection('vacancies').doc(id);
      await vacancyRef.get().then((doc) => {
        if (doc.exists) {
          if (doc.data().userId == userId) {
            exist = true;
          } else {
            throw new Error('Unauthorized user.');
          }
        }
      });

      if (exist) {
        await fireLib.db
          .collection('vacancies')
          .doc(id)
          .delete()
          .then(function (deleteDoc) {
            console.log('Document successfully deleted!');
            res.status(201).json({ id });
          })
          .catch(function (error) {
            console.error('Error removing document: ', error);
            throw new Error(error.message);
          });
      } else {
        throw new Error("There's no vacancy.");
      }
    } else {
      throw new Error('There is no ID');
    }
  } catch (e) {
    console.log(e);
    res.status(400).json({ error: e.message });
  }
});

module.exports.deleteVacancy = serverless(app);
