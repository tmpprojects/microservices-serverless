'use strict';

const fireLib = require(process.env.IS_OFFLINE
  ? process.env.FIREBASE_MODULE
  : 'firebaseServices');

const Sentry = require('@sentry/serverless');
Sentry.AWSLambda.init({
  dsn: 'https://eb8c2db2287943a19816ec13d7c6aeac@o312737.ingest.sentry.io/5244094',
  tracesSampleRate: 1.0,
});

const cors = require('cors');
const querystring = require('querystring');
const serverless = require('serverless-http');
const express = require('express');
const app = express();
const AWS = require('aws-sdk');
const bodyParser = require('body-parser');

const timeout = require('connect-timeout');
const moment = require('moment'); // require
require('moment-timezone'); // require
moment.locale('es-MX');
moment.tz.setDefault('America/Mexico_City');

app.use(express.urlencoded({ extended: true, strict: false }));
app.use(express.json());
app.use(cors());

const haltOnTimedout = (req, res, next) => {
  if (!req.timedout) {
    next();
  }
};
app.use(timeout(process.env.SETTIMEOUT));
app.use(haltOnTimedout);

app.get('/', async (req, res) => {
  res.json({ message: 'There is no such method.' });
});

app.post('/', async (req, res) => {
  try {


    /*********************************** SENTRY CONFIG ********************************/
    Sentry.setUser({
      email,
      //username: "No disponible",
      ip_address: req.ip,
    });
    Sentry.setContext('Más información del evento', {
      file_script: 'Login/RegisterCustomUser/handler.js',
      script_responsibility:
        'Crear una cuenta de usuario con un correo y una contraseña',
      req_body: req.body,
    });
    Sentry.setTag('lambda_section', 'Login');
    /*********************************** SENTRY CONFIG ********************************/
    
    res.status(201).json({ status: 'OK' });
  } catch (error) {
    console.log(error);
    res.status(400).json({ error: error.message });
      /* **************SENTRY REPORT********************** */
      Sentry.withScope((scope) => {
        scope.setLevel('error');
        Sentry.captureException(error);
      });
      /* **************SENTRY REPORT********************** */
  }
});

app.use((err, req, res, next) => {
  res.status(408).json({
    error: 'Nuestro servidor está tardando demasiado, intenta más tarde.',
  });
});

module.exports.authCustomUser = Sentry.AWSLambda.wrapHandler(serverless(app), {
  captureTimeoutWarning: false,
});



// app.use((err, req, res, next) => {
//   res.status(408).json({
//     error: process.env.ERROR_TIMEOUT,
//   });
//   /* **************SENTRY REPORT********************** */
//   Sentry.withScope((scope) => {
//     scope.setLevel('error');
//     Sentry.captureException(
//       new Error(process.env.ERROR_TIMEOUT),
//     );
//   });
//   /* **************SENTRY REPORT********************** */
// });

/*


//  res.status(400).json({
//       error: process.env.ERROR_QUOTE,
//     });
