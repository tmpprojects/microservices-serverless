'use strict';
const { firebase } = require(process.env.IS_OFFLINE
  ? process.env.FIREBASE_MODULE
  : 'firebaseServices');
process.setMaxListeners(20);
const Sentry = require('@sentry/serverless');
Sentry.AWSLambda.init({
  dsn: 'https://eb8c2db2287943a19816ec13d7c6aeac@o312737.ingest.sentry.io/5244094',
  tracesSampleRate: 1.0,
});

const cors = require('cors');
const querystring = require('querystring');
const serverless = require('serverless-http');
const express = require('express');
const app = express();
const AWS = require('aws-sdk');
const actions = require('./Actions/actions');
const timeout = require('connect-timeout');
const moment = require('moment'); // require
require('moment-timezone'); // require
moment.locale('es-MX');
moment.tz.setDefault('America/Mexico_City');

app.use(express.urlencoded({ extended: true, strict: false }));
app.use(express.json());
app.use(cors());

const haltOnTimedout = (req, res, next) => {
  if (!req.timedout) {
    next();
  }
};
app.use(timeout(process.env.SETTIMEOUT));
app.use(haltOnTimedout);

app.post('/', (req, res) => {
  res.status(405).json({
    response: process.env.HTTP_QUOTE,
  });
});

app.get('/', async (req, res) => {
  try {
    // Get the action to complete.
    /*********************************** SENTRY CONFIG ********************************/
    Sentry.setUser({
      //email,
      //username: "No disponible",
      ip_address: req.ip,
    });
    Sentry.setContext('Más información del evento', {
      file_script: 'Login/EmailActions/handler.js',
      script_responsibility:
        'Verificar email, resetear password y recuperar contraseña.',
      req_body: req.body,
    });
    Sentry.setTag('lambda_section', 'Login');
    /*********************************** SENTRY CONFIG ********************************/
    var mode = req.query.mode ? req.query.mode : null;
    var actionCode = req.query.oobCode ? req.query.oobCode : null;
    var _token = req.query._token ? req.query._token : null;
    var continueUrl = process.env.VERIFY_EMAIL;
    var lang = 'es';

    var auth = firebase.auth();
    var errorCatch = false;
    switch (mode) {
      case 'verifyEmail':
        // Display email verification handler and UI.
        await actions
          .handleVerifyEmail(auth, actionCode, continueUrl, lang)
          .then((e) => console.log('todo bien '));
        break;
      case 'resetPassword':
        // Display reset password handler and UI.
        await actions.handleResetPassword(auth, actionCode, continueUrl, lang);
        break;
      case 'recoverEmail':
        // Display email recovery handler and UI.
        await actions.handleRecoverEmail(auth, actionCode, lang);
        break;
      case 'forwardVerification':
        // Display email recovery handler and UI.
        await actions.handleForwardVerification(auth, _token);
        break;

      default:
      // Error: invalid mode.
    }
    res.status(201).json({ mode, continueUrl });
  } catch (error) {
    //console.log('Catch de error--->', error);
    res.status(400).json({ error: process.env.ERROR_QUOTE });
    /* **************SENTRY REPORT********************** */
    Sentry.withScope((scope) => {
      scope.setLevel('error');
      Sentry.captureException(error);
    });
    /* **************SENTRY REPORT********************** */
  }
});

//Si la llamada supera el tiempo máximo de la petición.
app.use((err, req, res, next) => {
  res.status(408).json({
    error: process.env.ERROR_TIMEOUT,
  });
  /* **************SENTRY REPORT********************** */
  Sentry.withScope((scope) => {
    scope.setLevel('error');
    Sentry.captureException(new Error(process.env.ERROR_TIMEOUT));
  });
  /* **************SENTRY REPORT********************** */
});

exports.actions = Sentry.AWSLambda.wrapHandler(serverless(app), {
  captureTimeoutWarning: false,
});

/**********************FIREBASE DOCUMENTARION*********************/

//https://firebase.google.com/docs/reference/android/com/google/firebase/auth/FirebaseAuth
//https://firebase.google.com/docs/auth/custom-email-handler

// firebase.auth().onAuthStateChanged(function (user) {
//   if (user) {
//     console.log('// User is signed in.', user);
//   } else {
//     console.log('No user is signed in.');
//     // No user is signed in.
//   }
// });

/*

firebase.auth().signOut().then(function() {
  // Sign-out successful.
}).catch(function(error) {
  // An error happened.
});

*/
