const { firebase, admin } = require(process.env.IS_OFFLINE
  ? process.env.FIREBASE_MODULE
  : 'firebaseServices');

const { nanoid } = require('nanoid');
const Sentry = require('@sentry/serverless');

function handleVerifyEmail(auth, actionCode, continueUrl, lang) {
  return new Promise((resolve, reject) => {
    auth
      .applyActionCode(actionCode)
      .then(function (resp) {
        console.log(resp);
        //admin.auth().updateUser(uid, { emailVerified: true })
        console.log(`Email validado, redireccionando a: ${continueUrl}`);

        resolve(true);
      })
      .catch(function (error) {
        // console.log(
        //   'El código es nulo o caducado. Pídele al usuario que verifique su dirección de correo electrónico ',
        //   error.message,
        // );

        /* **************SENTRY REPORT********************** */
        Sentry.withScope((scope) => {
          scope.setLevel('error');
          Sentry.captureException(error);
        });

        /* **************SENTRY REPORT********************** */
        reject(new Error('Ya se valido anteriormente esta cuenta.'));
      });
  });
}

function handleResetPassword(auth, actionCode, continueUrl, lang) {
  return new Promise((resolve, reject) => {
    // Localize the UI to the selected language as determined by the lang
    // parameter.
    var accountEmail;
    // Verify the password reset code is valid.
    auth
      .verifyPasswordResetCode(actionCode)
      .then(function (email) {
        var accountEmail = email;

        // TODO: Show the reset screen with the user's email and ask the user for
        // the new password.

        // Save the new password.
        auth
          .confirmPasswordReset(actionCode, newPassword)
          .then(function (resp) {
            // Password reset has been confirmed and new password updated.
            // TODO: Display a link back to the app, or sign-in the user directly
            // if the page belongs to the same domain as the app:
            // auth.signInWithEmailAndPassword(accountEmail, newPassword);
            // TODO: If a continue URL is available, display a button which on
            // click redirects the user back to the app via continueUrl with
            // additional state determined from that URL's parameters.
            resolve(true);
          })
          .catch(function (error) {
            // Error occurred during confirmation. The code might have expired or the
            // password is too weak.
            reject(
              new Error(
                'El código es nulo o caducado. Pídele al usuario que verifique su dirección de correo electrónico',
              ),
            );
          });
      })
      .catch(function (error) {
        // Invalid or expired action code. Ask user to try to reset the password
        // again.
        reject(
          new Error(
            'El código es nulo o caducado. Pídele al usuario que verifique su dirección de correo electrónico',
          ),
        );
      });
  });
}

function handleRecoverEmail(auth, actionCode, lang) {
  // Localize the UI to the selected language as determined by the lang
  // parameter.
  var restoredEmail = null;
  // Confirm the action code is valid.
  auth
    .checkActionCode(actionCode)
    .then(function (info) {
      // Get the restored email address.
      restoredEmail = info['data']['email'];

      // Revert to the old email.
      return auth.applyActionCode(actionCode);
    })
    .then(function () {
      // Account email reverted to restoredEmail

      // TODO: Display a confirmation message to the user.

      // You might also want to give the user the option to reset their password
      // in case the account was compromised:
      auth
        .sendPasswordResetEmail(restoredEmail)
        .then(function () {
          // Password reset confirmation sent. Ask user to check their email.
        })
        .catch(function (error) {
          // Error encountered while sending password reset code.
        });
    })
    .catch(function (error) {
      // Invalid code.
    });
}

function handleForwardVerification(auth, _token) {
  console.log("forwardVerification", _token)
  firebase
    .auth()
    .signInWithCustomToken(_token)
    .then((user) => {
      // Signed in
      // ...
      //console.log('user')
      user['mode'] = 'token';
      //res.status(201).json(user);

      //console.log("se valido el token", _token)

      firebase
      .auth()
      .currentUser.sendEmailVerification()
      .then((e) => console.log(`Se envío correo de verificación a ${user.user.email}`))
      .catch((error) => {
        console.log(
          `No fue posible mandar correo a verificación a ${user.user.email}`,
          error.message,
        );

        /* **************SENTRY REPORT********************** */
        Sentry.withScope((scope) => {
          scope.setLevel('error');
          Sentry.captureException(error);
        });
        /* **************SENTRY REPORT********************** */
      });

      //res.status(201).json(user);


    })
    .catch((error) => {
      console.log(error.message);
      
    });
}

module.exports = {
  handleVerifyEmail,
  handleResetPassword,
  handleRecoverEmail,
  handleForwardVerification,
};

/**********************FIREBASE DOCUMENTARION*********************/

//https://firebase.google.com/docs/reference/android/com/google/firebase/auth/FirebaseAuth
//https://firebase.google.com/docs/auth/custom-email-handler
