function getError(errorCode){
    let errorMessageResponse = "";
        switch (errorCode) {
          case "auth/email-already-in-use":
            errorMessageResponse =
              "La cuenta que intentas crear ya esta en uso por otra persona.";

            break;
          case "auth/invalid-email":
            errorMessageResponse = "Verifica tu correo.";

            break;
          case "auth/operation-not-allowed":
            errorMessageResponse =
              "La operación de crear cuenta no esta permitida.";

            break;
          case "auth/weak-password":
            errorMessageResponse =
              "Inténtalo de nuevo con una contraseña más segura.";

            break;
          default:
            errorMessageResponse =
              "* Ocurrió un error al intentar crear tu cuenta.";
        }
        return errorMessageResponse;
}
module.exports = {
    getError
}