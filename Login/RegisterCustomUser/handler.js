'use strict';

const { firebase } = require(process.env.IS_OFFLINE
  ? process.env.FIREBASE_MODULE
  : 'firebaseServices');

const Sentry = require('@sentry/serverless');
Sentry.AWSLambda.init({
  dsn: 'https://eb8c2db2287943a19816ec13d7c6aeac@o312737.ingest.sentry.io/5244094',
  tracesSampleRate: 1.0,
});

const cors = require('cors');
const customErrors = require('./errors');
const serverless = require('serverless-http');
const express = require('express');
const app = express();
const AWS = require('aws-sdk');
const bodyParser = require('body-parser');

const timeout = require('connect-timeout');
const moment = require('moment'); // require
require('moment-timezone'); // require
moment.locale('es-MX');
moment.tz.setDefault('America/Mexico_City');


app.use(express.urlencoded({ extended: true, strict: false }));
app.use(express.json());
app.use(cors());

const haltOnTimedout = (req, res, next) => {
  if (!req.timedout) {
    next();
  }
};
app.use(timeout(process.env.SETTIMEOUT));
app.use(haltOnTimedout);


app.get('/', (req, res) => {
  res.status(405).json({
    response: process.env.HTTP_QUOTE,
  });
});

app.post('/', (req, res) => {
  try {
    const email = req.body.email ? req.body.email : null;
    const password = req.body.password ? req.body.password : null;

    /*********************************** SENTRY CONFIG ********************************/
    Sentry.setUser({
      email,
      //username: "No disponible",
      ip_address: req.ip,
    });
    Sentry.setContext('Más información del evento', {
      file_script: 'Login/RegisterCustomUser/handler.js',
      script_responsibility:
        'Crear una cuenta de usuario con un correo y una contraseña',
      req_body: req.body,
    });
    Sentry.setTag('lambda_section', 'Login');
    /*********************************** SENTRY CONFIG ********************************/

    //Validar que el usuario y la contraseña sean correctos.
    if (email && password) {
      //Iniciar el proceso de creación de cuenta

      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then((user) => {
          //Si se logró crear correctamente la cuenta enviar un correo electrónico para verificar la cuenta
          firebase
            .auth()
            .currentUser.sendEmailVerification()
            .then((e) => console.log(`Se envío correo de verificación a ${email}`))
            .catch((error) => {
              console.log(
                `No fue posible mandar correo a verificación a ${email}`,
                error.message,
              );

              /* **************SENTRY REPORT********************** */
              Sentry.withScope((scope) => {
                scope.setLevel('error');
                Sentry.captureException(error);
              });
              /* **************SENTRY REPORT********************** */
            });

          //Enviar la respuesta y el token de sesión generado.
          user.user.getIdToken().then((token) => {
            res.status(200).json({ uid: user.user.uid, email: email, token });
          });
        })
        .catch((error) => {
          //Si NO se logró crear correctamente la cuenta
          const errorCode = error.code;
          const errorMessage = error.message;

          console.log(errorCode);
          console.log(errorMessage);

          res
            .status(401)
            .json({ error: error.message });

          /* **************SENTRY REPORT********************** */

          Sentry.withScope((scope) => {
            scope.setLevel('error');
            Sentry.captureException(error);
          });
          /* **************SENTRY REPORT********************** */
        });
    } else {
      //No viene algún parametro o uno de los datos es incorrecto (hacking)
      res.status(400).json({
        response: 'El usuario o la contraseña no son correctos.',
      });

      /* **************SENTRY REPORT********************** */
      Sentry.withScope((scope) => {
        scope.setLevel('info');
        Sentry.captureException(
          new Error('El usuario o la contraseña no son correctos o falta alguno.'),
        );
      });
      /* **************SENTRY REPORT********************** */
    }
  } catch (error) {
    //Ocurrió un error desconocido a nivel de servidor.
    console.log(error.message);
    res.status(400).json({
      error: error.message,
    });

    /* **************SENTRY REPORT********************** */
    Sentry.withScope((scope) => {
      scope.setLevel('error');
      Sentry.captureException(error.message);
    });
    /* **************SENTRY REPORT********************** */
  }
});

//Si la llamada supera el tiempo máximo de la petición.
app.use((err, req, res, next) => {
  res.status(408).json({
    error: process.env.ERROR_TIMEOUT,
  });
  /* **************SENTRY REPORT********************** */
  Sentry.withScope((scope) => {
    scope.setLevel('error');
    Sentry.captureException(
      new Error(process.env.ERROR_TIMEOUT),
    );
  });
  /* **************SENTRY REPORT********************** */
});

exports.registerCustomUser = Sentry.AWSLambda.wrapHandler(serverless(app), {
  captureTimeoutWarning: false,
});

/*

Notas:

Sentry: https://docs.sentry.io/platforms/node/guides/aws-lambda/

//VERIFICAR EMAIL admin.auth().updateUser(uid, { emailVerified: true })

    // firebase.auth().onAuthStateChanged(function (user) {
    //   if (user) {
    //     // User is signed in.
    //     console.log("User is signed in 💫.");
    //   } else {
    //     // No user is signed in.
    //     console.log("No user is signed in.");
    //   }
    // });
    // const user = firebase.auth().currentUser;
*/
