'use strict';

const { firebase, admin } = require(process.env.IS_OFFLINE
  ? process.env.FIREBASE_MODULE
  : 'firebaseServices');

const Sentry = require('@sentry/serverless');
Sentry.AWSLambda.init({
  dsn: 'https://eb8c2db2287943a19816ec13d7c6aeac@o312737.ingest.sentry.io/5244094',
  tracesSampleRate: 1.0,
  environment: process.env.STAGE,
});

const cors = require('cors');
const querystring = require('querystring');
const serverless = require('serverless-http');
const express = require('express');
const app = express();
const AWS = require('aws-sdk');
const bodyParser = require('body-parser');

const timeout = require('connect-timeout');
const moment = require('moment'); // require
require('moment-timezone'); // require
moment.locale('es-MX');
moment.tz.setDefault('America/Mexico_City');

app.use(express.urlencoded({ extended: true, strict: false }));
app.use(express.json());
app.use(cors());

const haltOnTimedout = (req, res, next) => {
  if (!req.timedout) {
    next();
  }
};
app.use(timeout(process.env.SETTIMEOUT));
app.use(haltOnTimedout);

app.get('/', (req, res) => {
  console.log('ENV--->', process.env);
  res.status(405).json({
    errror: process.env.HTTP_QUOTE,
  });
});

app.post('/', async (req, res) => {
  try {
    console.log(process.env.STAGE);
    const email = req.body.email ? req.body.email : null;
    const password = req.body.password ? req.body.password : null;
    const token = req.body.token ? req.body.token : null;

    /*********************************** SENTRY CONFIG ********************************/
    Sentry.setUser({
      email,
      password,
      ip_address: req.ip,
    });
    Sentry.setContext('Más información del evento', {
      file_script: 'Login/AuthCustomUser/handler.js',
      script_responsibility: 'Iniciar sesión con usuario y contraseña.',
      req_body: req.body,
    });
    Sentry.setTag('lambda_section', 'Login');
    /*********************************** SENTRY CONFIG ********************************/

    if (token) {
      console.log(token);

      firebase
        .auth()
        .signInWithCustomToken(token)
        .then((user) => {
          // Signed in
          // ...
          user['mode'] = 'token';
          res.status(201).json(user);
        })
        .catch((error) => {
          console.log(error.messaege);
          res.status(408).json({
            error:
              process.env.STAGE == 'prod' ? process.env.ERROR_QUOTE : error.message,
          });
        });

    

      //  admin
      // .auth()
      // .verifyIdToken(token)
      // .then((decodedToken) => {
      //   const uid = decodedToken.uid;
      //   // ...
      //   res.status(201).json(decodedToken);
      // })
      // .catch((error) => {
      //   // Handle error

      //   res.status(201).json(error.message);
      // });
    } else {
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(async (user) => {
          // Signed in
          // ...
          user['mode'] = 'normal';
          console.log(user);

          //const idToken = await firebase.auth().currentUser.getIdToken()

          // res.status(200).json({ response: email, idToken });
          const idToken = await firebase.auth().currentUser.getIdToken()
          //user['token'] = idToken;
          ///res.status(201).json(user);

          const userId = user.user.uid;
          /////////
          admin
            .auth()
            .createCustomToken(userId)
            .then((customToken) => {
              user['_token'] = customToken;
           
              res.status(201).json(user);
            })
            .catch((error) => {
              res
                .status(400)
                .json({ error: '[001] Ocurrió un error al intentar iniciar sesión.' });
              console.log('Error creating custom token:', error);
            });
        })
        .catch((error) => {
          console.log('info->', error.message);
          /* **************SENTRY REPORT********************** */
          Sentry.withScope((scope) => {
            scope.setLevel('error');
            Sentry.captureException(new Error(error));
          });
          /* **************SENTRY REPORT********************** */
          res.status(408).json({
            error: '[002] Ocurrió un error al intentar iniciar sesión.' 
          });
        });
    }
  } catch (error) {
    console.log(error.message);
    res.status(408).json({
      error: process.env.STAGE == 'prod' ? process.env.ERROR_QUOTE : error.message,
    });
    /* **************SENTRY REPORT********************** */
    Sentry.withScope((scope) => {
      scope.setLevel('info');
      Sentry.captureException(new Error(error.message));
    });
    /* **************SENTRY REPORT********************** */
  }
});

app.use((err, req, res, next) => {
  res.status(408).json({
    error: process.env.ERROR_TIMEOUT,
  });
  /* **************SENTRY REPORT********************** */
  Sentry.withScope((scope) => {
    scope.setLevel('error');
    Sentry.captureException(new Error(process.env.ERROR_TIMEOUT));
  });
  /* **************SENTRY REPORT********************** */
});

module.exports.authCustomUser = Sentry.AWSLambda.wrapHandler(serverless(app), {
  captureTimeoutWarning: false,
});
