'use strict';

const cors = require('cors');

const fireLib =  require(process.env.IS_OFFLINE
  ? process.env.FIREBASE_MODULE
  : "firebaseServices");
const querystring = require('querystring');
const serverless = require('serverless-http');
const express = require('express');
const app = express();
const AWS = require('aws-sdk');
const bodyParser = require('body-parser');
const moment = require('moment'); // require
require("moment-timezone"); // require
moment.locale('es-MX');
moment.tz.setDefault('America/Mexico_City');

const path = require('path');


app.use(express.urlencoded({ extended: true, strict: false }));
app.use(express.json());
app.use(cors());



// Add the Firebase products that you want to use



const OFFLINE = process.env.IS_OFFLINE;
let priorityMissing;


app.get('/', async (req, res) => {

  res.sendFile(path.join(__dirname + '/redirect.html'));

});



module.exports.authGoogle = serverless(app);
