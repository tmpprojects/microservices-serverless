'use strict';

const cors = require('cors');
const fireLib =  require(process.env.IS_OFFLINE
  ? process.env.FIREBASE_MODULE
  : "firebaseServices");
const querystring = require('querystring');
const serverless = require('serverless-http');
const express = require('express');
const app = express();
const AWS = require('aws-sdk');
const bodyParser = require('body-parser');
const moment = require('moment'); // require
require("moment-timezone"); // require
moment.locale('es-MX');
moment.tz.setDefault('America/Mexico_City');

const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

app.use(express.urlencoded({ extended: true, strict: false }));
app.use(express.json());
app.use(cors());



// app.get('/', async (req, res) => {
//   res.json({ message: 'There is no such method.' });
// });
// app.post('/', async (req, res) => {
//   res.json({ message: 'There is no such method.' });
// });

app.get('/', async (req, res) => {

  let password =  await bcrypt.hash('password',1);
  let token = await jwt.sign({ foo: 'bar' },'secreto', { expiresIn: '0' } )

  bcrypt.compare('password', password).then(sonIguales => {
    if(sonIguales===true){
      res.status(201).json({token,password});
    }else{
      res.status(201).json('no son nada parecidas');
    }
  
  }).catch(noSon => {
    res.status(201).json('La pass esta mal');
  });
 
});

module.exports.authJWT = serverless(app);
