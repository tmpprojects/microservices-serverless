'use strict';

const cors = require('cors');
const fireLib = require(process.env.IS_OFFLINE
  ? process.env.FIREBASE_MODULE
  : 'firebaseServices');

const serverless = require('serverless-http');
const express = require('express');
const app = express();
//const AWS = require('aws-sdk');

const md5 = require('md5'); // require
const sanitizeHtml = require('sanitize-html'); // require
const moment = require('moment'); // require
require('moment-timezone'); // require
moment.tz.setDefault('America/Mexico_City');
moment.locale('es-MX');

app.use(express.urlencoded({ extended: true, strict: false }));
app.use(cors());
app.use(express.json());

const OFFLINE = process.env.IS_OFFLINE;
let priorityMissing;

function sanitizeComment(comment) {
  let returnComment = sanitizeHtml(comment);
  returnComment = returnComment.slice(0, 500);
  return returnComment;
}

app.get('/', async (req, res) => {
  res.json({ message: 'There is no such method.' });
});

app.delete('/', async (req, res) => {
  const { idVacancy, idComment } = req.body;
  let concentrateComments = {};

  //Get previous comments
  const docRef = await fireLib.db.collection('vacancies').doc(idVacancy);
  await docRef.get().then(function (doc) {
    if (doc.exists) {
      //Save previous comments
      // check  doc.data().comments or  doc.data();
      concentrateComments = doc.data().comments;
    }
  });

  const deleteComment = concentrateComments.filter(function (obj) {
    return obj.idComment !== idComment;
  });

  const result = await fireLib.db.collection('vacancies').doc(idVacancy).set(
    {
      comments: deleteComment,
    },
    { merge: true },
  );

  const finishResult = await docRef.get();
  let printResult = {};
  if (!finishResult.exists) {
    console.log('No such document!');
  } else {
    printResult = finishResult.data();
  }

  res.status(200).json(printResult);
});

app.post('/', async (req, res) => {

  try {
    const { comment, parentId, type, user } = req.body;
    if (comment && parentId && type) {
      let concentrateComments = [];
      let postInfo;

      if (Boolean(comment) == false) {
        res.status(400).json({ error: 'Empty comment.' });
        return true;
      }

      //Get previous comments
      let docRef = await fireLib.db.collection('vacancies').doc(parentId);
      await docRef.get().then(function (doc) {
        if (doc.exists) {
          //Save previous comments
          postInfo = doc.data();
          concentrateComments = doc.data().comments;
        }
      });

      concentrateComments.push({
        parentId: parentId,
        owner:{
          name: user.name,
          title: user.title,
          imageProfile: user.imageProfile,
          idUser: user.idUser,
        },
        comment: sanitizeComment(comment),
        idComment: md5(`${user.idUser}${parentId}${moment().toDate()}`),
        createAt: moment().toDate(),
        likes: {},
        type,
      });

      await fireLib.db
        .collection('vacancies')
        .doc(parentId)
        .set(
          {
            comments: concentrateComments,
          },
          { merge: true },
        )
        .then(function (created) {
          res.status(201).json(concentrateComments);
        })
        .catch(function (e) {
          res.status(400).json({ error: e.message });
          throw new Error(error.message);
        });
    } else {
      throw new Error("There's data missing");
    }
  } catch (e) {
    res.status(400).json({ error: e.message });
  }
});

app.post('/spam', async (req, res) => {
  try {
    const { comment, parentId, type, user } = req.body;
    if (comment && parentId && type) {
      let concentrateComments = [];
      let postInfo;

      // fireLib.db.collection('logs').doc().set({
      //   idUser: user.idUser,
      //   parentId: parentId,
      //   title: postInfo.title,
      //   tags: postInfo.tags,
      //   collection: 'vacancies',
      //   path: null,
      //   action: 'Spam in comments.',
      //   createAt: moment().toDate(),
      //   dangerous: 'middle',
      // });

      //Notify email:

      res.status(201).json({ message: 'User broke the rules.' });
    } else {
      throw new Error("There's data missing");
    }
  } catch (e) {
    res.status(400).json({ error: e.message });
  }
});

module.exports.comments = serverless(app);
