'use strict';
const cors = require('cors');
const Sentry = require('@sentry/serverless');
const fireLib = require(process.env.IS_OFFLINE
  ? process.env.FIREBASE_MODULE
  : 'firebaseServices');
const serverless = require('serverless-http');
const express = require('express');
const app = express();
const moment = require('moment'); // require
require('moment-timezone'); // require
moment.locale('es-MX');
moment.tz.setDefault('America/Mexico_City');

app.use(express.urlencoded({ extended: true, strict: false }));
app.use(express.json());
app.use(cors());
process.setMaxListeners(0);

Sentry.AWSLambda.init({
  dsn: 'https://f8c5e59950fa48ceb01cf668fa8fdbce@o312737.ingest.sentry.io/5668999',
  // We recommend adjusting this value in production, or using tracesSampler
  // for finer control
  tracesSampleRate: 1.0,
});

function returnObjects(snapshot) {
  const objArray = [];
  snapshot.forEach(function (favs) {
    objArray.push(favs.data());
  });
  return objArray;
}

app.get('/', async (req, res) => {





  //////////////
  const cityRef = fireLib.db.doc('comments/UOgYCoSEqEMU5yWkw3Mu');
  //const cityRef = fireLib.db.doc('comments/UOgYCoSEqEMU5yWkw3Mu/favorites/hdakDpwd4ubDq4HfiJ1D');

  const doc = await cityRef.get();
  if (!doc.exists) {
    console.log('No such document!');
  } else {
    const favs = await cityRef
      .collection('favorites')
      .get()
      .then((snap) => returnObjects(snap));

    console.log(favs);
    //querySnapshot.forEach(function(doc) { doc.data() doc.id

    // console.log('Document data:', doc.data());
    res.json(doc.data());
  }
  // res.json({ message: 'There is no such method.' });
});

app.post('/', async (req, res) => {
  try {
    const { user, parentId, title, subCategories, fav } = req.body;

    //////////////////////////////////////

    if (fav) {
      await fireLib.db
        .collection('favorites')
        .doc(parentId)
        .set({
          parentId,
          title,
          subCategories,
          owner: user,
        })
        .then((result) => {
          console.log('created');
          res.status(201).json('created');
        })
        .catch((e) => {
          console.log('error');
          res.status(400).json(e.message);
        });
    } else {
      await fireLib.db.collection('favorites').doc(parentId).delete();
      console.log('deleted');
      res.status(200).json('deleted');
    }

    //////////////////////////////////////

    // if (!newFavorite) {
    //   console.log('new favorite');

    //   await fireLib.db
    //     .collection('favorites')
    //     .doc(parentId)
    //     .set({
    //       parentId,
    //       title,
    //       subCategories,
    //       owner: user,
    //     })
    //     .then(async (docRef) => {
    //       //created.push({ id: docRef.id });

    //       await fireLib.db
    //         .collection('vacancies')
    //         .doc(parentId)
    //         .set(
    //           {
    //             favorites: [
    //               {
    //                 createAt: moment().toDate(),
    //                 owner: {
    //                   name: user.name,
    //                   title: user.title,
    //                   idUser: user.idUser,
    //                 },
    //               },
    //             ],
    //           },
    //           { merge: true },
    //         )
    //         .then(function (docRef) {
    //           //created.push({ id: docRef.id });
    //           res.status(201).json('created');
    //         })
    //         .catch(function (error) {
    //           res.status(400).json(error.message);
    //         });
    //     })
    //     .catch(function (error) {
    //       res.status(400).json(error.message);
    //     });
    //   res.status(200).json({ res: 'ok' });
    // }
  } catch (e) {
    console.log(e);
    res.status(400).json({ error: e.message });
  }
});

module.exports.favorites = Sentry.AWSLambda.wrapHandler(serverless(app));
