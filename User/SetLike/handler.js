'use strict';
const cors = require('cors');
const fireLib = require(process.env.IS_OFFLINE
  ? process.env.FIREBASE_MODULE
  : 'firebaseServices');
const querystring = require('querystring');
const serverless = require('serverless-http');
const express = require('express');
const app = express();
const AWS = require('aws-sdk');
const bodyParser = require('body-parser');
const moment = require('moment'); // require
const md5 = require('md5'); // require
const sanitizeHtml = require('sanitize-html'); // require
require('moment-timezone'); // require
moment.tz.setDefault('America/Mexico_City');
moment.locale('es-MX');

app.use(express.urlencoded({ extended: true, strict: false }));
app.use(cors());
app.use(express.json());

const OFFLINE = process.env.IS_OFFLINE;
let priorityMissing;

app.get('/', async (req, res) => {
  res.json({ message: 'There is no such method. Like' });
});

app.post('/', async (req, res) => {
  try {
    const { user, parentId, heartLike } = req.body;

    if (user.idUser && parentId) {
      let concentrateLikes = [];
      //Get previous likes
      const docRef = await fireLib.db.collection('vacancies').doc(parentId);
      await docRef.get().then(function (doc) {
        if (doc.exists) {
          //Save previous likes
          concentrateLikes = doc.data().likes;
        }
      });

      const filterLikes = concentrateLikes.filter(function (obj) {
        return obj.owner.idUser !== user.idUser;
      });

      if (heartLike === true) {
        // Attach like
        filterLikes.push({
          parentId: parentId,
          createAt: moment().toDate(),
          owner: { ...user },
        });
        console.log('Add like.', heartLike);
      }

      await fireLib.db
        .collection('vacancies')
        .doc(parentId)
        .set(
          {
            likes: filterLikes,
          },
          { merge: true },
        )
        .then(function (created) {
          res.status(201).json(filterLikes);
        })
        .catch(function (e) {
          res.status(400).json({ error: e.message });
          throw new Error(error.message);
        });
    } else {
      console.log(req.body);
      res.status(400).json({ error: "There's data missing" });
      throw new Error("There's data missing");
    }
  } catch (e) {
    res.status(400).json({ error: e.message });
  }
});

module.exports.setLike = serverless(app);
