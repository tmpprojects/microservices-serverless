'use strict';

const cors = require('cors');
const fireLib = require(process.env.IS_OFFLINE
  ? process.env.FIREBASE_MODULE
  : 'firebaseServices');
//const querystring = require('querystring');
const serverless = require('serverless-http');
const express = require('express');
const app = express();
// const AWS = require('aws-sdk');
// const bodyParser = require('body-parser');

// const moment = require('moment'); // require
// require('moment-timezone'); // require
// moment.locale('es-MX');
// moment.tz.setDefault('America/Mexico_City');

app.use(express.urlencoded({ extended: true, strict: false }));
app.use(express.json());
app.use(cors());

const OFFLINE = process.env.IS_OFFLINE;

function returnObjects(snapshot) {
  const objArray = [];
  snapshot.forEach(function (favs) {
    objArray.push({ ...favs.data(), id: favs.id });
  });
  return objArray;
}

app.get('/', async (req, res) => {
  try {
    const defaultLimit = 20;
    let limit = req.query.limit ? req.query.limit : defaultLimit;

    //Obetener todas las vacantes
    const documents = await fireLib.db
      .collection('vacancies')
      .get()
      .then((querySnapshot) => returnObjects(querySnapshot));

    //Iterar en las vacantes para obtener su id e inyectar los sub collecciones
    for (let index in documents) {
      const id = documents[index].id;

      //Adjuntar la sub colección de likes
      const likes = await fireLib.db
        .collection('vacancies')
        .doc(id)
        .collection('likes')
        .get()
        .then((querySnapshot) => returnObjects(querySnapshot));

      //Adjuntar la sub colección de applyes
      const applyes = await fireLib.db
        .collection('vacancies')
        .doc(id)
        .collection('applyes')
        .get()
        .then((querySnapshot) => returnObjects(querySnapshot));

      //Adjuntar la sub colección de comments
      const comments = await fireLib.db
        .collection('vacancies')
        .doc(id)
        .collection('comments')
        .get()
        .then((querySnapshot) => returnObjects(querySnapshot));

      //Adjuntar la sub colección de favorites
      const favorites = await fireLib.db
        .collection('vacancies')
        .doc(id)
        .collection('favorites')
        .get()
        .then((querySnapshot) => returnObjects(querySnapshot));

      //Adjuntar todo a la colección iterada
      documents[index] = {
        ...documents[index],
        likes,
        applyes,
        comments,
        favorites,
      };
    }

    res.status(200).json(documents);
  } catch (e) {
    res.status(400).json({ error: e.message });
  }
});

module.exports.getVacancies = serverless(app);
