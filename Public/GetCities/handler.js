'use strict';

const cors = require('cors');
const fireLib = require(process.env.IS_OFFLINE
  ? process.env.FIREBASE_MODULE
  : 'firebaseServices');
const serverless = require('serverless-http');
const express = require('express');
const app = express();
//const AWS = require('aws-sdk');
// const moment = require('moment'); // require
// require('moment-timezone'); // require
// moment.locale('es-MX');
// moment.tz.setDefault('America/Mexico_City');

app.use(express.urlencoded({ extended: true, strict: false }));
app.use(express.json());
app.use(cors());

app.get('/', async (req, res) => {
  try {
    const defaultLimit = 20;
    let limit = req.query.limit ? req.query.limit : defaultLimit;
    let yourData = [];
    let cityRef = await fireLib.db
      .collection('cities')
      .limit(limit)
      .get()
      .then((data) => {
        const temp = [];
        const response = data.forEach((doc) => {
          temp.push(doc.data());
        });
        return temp;
      });

    // so here the yourData is iterable... here comes the part you just mentioned
    // for (let doc of yourData) {
    //   console.log(doc); // each element is here!
    // }

    res.status(200).json(cityRef);
    // .then((snapshot) => {
    //   // snapshot.forEach((doc) => {
    //   //   console.log(doc.id, '=>', doc.data());
    //   // });
    //   return res.json(snapshot);
    // })
    // .catch((err) => {
    //   console.log('Error getting documents', err);
    // });

    // query.get().then((querySnapshot) => {
    //   querySnapshot.forEach((documentSnapshot) => {
    //     console.log(`Found document at ${documentSnapshot.ref.path}`);
    //     return res.json(documentSnapshot.data());
    //   });
    // });

    /*
    let query = firebaseLib.firestore
      .collection('vacancies')
      .where('title', '==', 'programador web');

    query.get().then((querySnapshot) => {
      querySnapshot.forEach((documentSnapshot) => {
        console.log(`Found document at ${documentSnapshot.ref.path}`);
        return res.json(documentSnapshot.data());
      });
    });
    */

    /*
    let citiesRef = firebaseLib.db_admin.collection('vacancies');
    let query = citiesRef
      .orderBy('title')
      .startAt(2)
      .limit(1)
      .get()
      .then((snapshot) => {
        if (snapshot.empty) {
          console.log('No matching documents.');
          return res.json({ error: true });
        }

        let demo = [];
        snapshot.forEach((doc) => {
          console.log(doc.id, '=>', doc.data());
          demo.push({
            id: doc.id,
            data: doc.data(),
          });
        });
        res.json(demo);
      })
      .catch((err) => {
        console.log('Error getting documents', err);
        return res.json({ error: true });
      });
      */
  } catch (e) {
    res.status(400).json({ error: e.message });
  }
});

module.exports.getCities = serverless(app);
